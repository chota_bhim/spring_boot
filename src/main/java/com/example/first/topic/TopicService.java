package com.example.first.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {
	
	

	 private List<Topics> topics =  new ArrayList<>(Arrays.asList(
			new Topics("1","Spring"),
			new Topics("2","java"),
			new Topics("3","python")
			));
	 
	 public List<Topics> getAllTopics()
	 {
		 return topics;
	 }
	 
	 public Topics getTopic(String id)
	 {
		 return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
	 }

	public void addTopic(Topics topic) {
		 topics.add(topic);
		
	}

	public void updateTopic(String id, Topics topics2) {
		for (int i=0;i<topics.size();i++)
		{
			Topics t = topics.get(i);
			if(t.getId().equals(id))
			{
				topics.set(i, topics2);
			}
		}
		
	}

	public void deleteTopic(String id) {
		for (int i=0;i<topics.size();i++)
		{
			Topics t = topics.get(i);
			if(t.getId().equals(id))
			{
				topics.remove(i);
			}
		}
		
	}
	 
	 
}
